"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import os
from pathlib import Path

from picard_watcher.utils import Event


def CommaSeparatedList(arg: str):
    return arg.split(",")


class PathWithCondition:
    def __init__(self, func_name: str, error_message: str, is_executable: bool = None):
        self.func_name = func_name
        self.error_message = error_message
        self.is_executable = is_executable

    def __call__(self, arg: str) -> Path:
        path = Path(arg)
        if not getattr(path, self.func_name)():
            raise argparse.ArgumentTypeError(self.error_message)
        if self.is_executable is not None and os.access(path, os.X_OK) is not self.is_executable:
            raise argparse.ArgumentTypeError("Path is not executable")

        return path


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="picard_watcher",
        description="Execute picard script on music files in a watched folder",
    )
    parser.add_argument(
        "-v", "--verbose",
        help="Enable verbose / debug logging",
        action="store_true",
    )
    parser.add_argument(
        "-e", "--endings",
        help="Comma-separated list of file endings/extensions to be considered (without leading dot)",
        type=CommaSeparatedList,
        default=["mp3", "m4a", "ogg", "flac"]
    )
    parser.add_argument(
        "-t", "--tag",
        help="Tags that are required for the audio file to be considered for renaming",
        action="append",
        dest="tags",
    )
    parser.add_argument(
        "--handler",
        type=PathWithCondition("is_file", "Expected an executable file", is_executable=True),
        help="Executable scripts to call when events are emitted. "
             "Args passed to scripts: <event_type> <arg_count> [...args] . "
             f"Possible event types are: {','.join(Event)}",
    )
    parser.add_argument(
        "watch_dir",
        help="The folder to watch/monitor",
        type=PathWithCondition("is_dir", "Expected a existing directory"),
    )
    parser.add_argument(
        "script",
        help="File containing picard script",
        type=PathWithCondition("is_file", "Expected a existing file"),
    )
    parser.add_argument(
        "target",
        help="Root directory for where good files will be moved",
        type=PathWithCondition("is_dir", "Expected a existing directory"),
    )
    return parser
