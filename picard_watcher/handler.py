"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import hashlib
import logging
import shutil
import subprocess
from pathlib import Path
from typing import Iterable

from picard.script import ScriptParser
from watchdog.events import FileClosedEvent, FileMovedEvent, PatternMatchingEventHandler

from picard_watcher.errors import (
    MissingRequiredTagsError, NewPathExistsError, NoNewPathError,
    NoTagsError, PicardWatcherError,
)
from picard_watcher.utils import Event, exec_script, get_tags, remove_empty_dirs_until_parent

logger = logging.getLogger(__name__)


class PicardRenameEventHandler(PatternMatchingEventHandler):

    def __init__(
            self,
            watch_dir: Path,
            script: str,
            target_dir: Path,
            endings: Iterable[str] = None,
            required_tags: Iterable[str] = None,
            event_handler: Path = None,
            **kwargs
    ):
        """
        :param watch_dir: The directory being monitored.
        :param script: The picard Foo script to execute in order to get a new path for a handled file
        :param target_dir: The root directory where to put the handled file
        :param endings: The file endings that will be considered when handling a file
        :param required_tags: The necessary music (ID3, or whatever) tags a file should have
        :param event_handler: A script to call when an internal event occurs (error, move, etc.)
        :param kwargs: args passed on to the super-class
        """
        super().__init__(**kwargs)
        self.watch_dir = watch_dir
        self.target_dir = target_dir
        self.parser = ScriptParser()
        self.script = script
        self.endings = endings or []
        self.required_tags = required_tags
        self.event_handler = event_handler

    def call_event_listener(self, event: Event, *args: str):
        if not self.event_handler:
            return
        if subprocess.call([self.event_handler, event.value] + list(args)) != 0:
            logger.warning("Error calling event_handler: %s", [event.value] + list(args))

    def handle_event_path(self, path: Path):
        if path.is_dir():
            return

        if self.endings and not any(path.suffix.lower() == ("." + ending.lower()) for ending in self.endings):
            logger.info("Ignoring: %s", path)
            return
        try:
            self.handle_file(path)
        except PicardWatcherError as watcher_error:
            rel_path = str(path.relative_to(self.watch_dir))
            logger.warning("Error while handling %s: %s", rel_path, watcher_error)
            self.call_event_listener(
                Event.ERROR,
                watcher_error.code.value,
                str(watcher_error),
                rel_path,
            )
        except:
            logger.exception("Couldn't treat file: %s", path)

    def handle_file(self, file_path: Path):
        logger.debug("Handling file: %s", file_path)
        tags = get_tags(file_path)
        if not tags:
            raise NoTagsError()

        if not self.has_required_tags(tags):
            raise MissingRequiredTagsError(self.required_tags)

        # Calc new path
        new_path_rel = exec_script(self.script, tags)
        if not new_path_rel:
            raise NoNewPathError()

        new_path = self.target_dir / Path(new_path_rel + file_path.suffix)
        if new_path.exists():
            self.handle_existing_path(file_path, new_path)
        else:
            # Move file
            new_path.parent.mkdir(parents=True, exist_ok=True)
            shutil.move(file_path, new_path)
            logger.info("Moved %s to %s", file_path, new_path)
            self.call_event_listener(Event.MOVED, str(file_path), str(new_path))

        # Clean up
        remove_empty_dirs_until_parent(file_path.parent, self.watch_dir)

    def has_required_tags(self, tags: dict):
        if self.required_tags:
            return set(self.required_tags).intersection(tags.keys())
        return True

    def on_closed(self, event: FileClosedEvent):
        self.handle_event_path(Path(event.src_path))

    def on_moved(self, event: FileMovedEvent):
        # Only consider files moved/renamed in or into our watched directory
        dest_path = event.dest_path
        if not dest_path.startswith(str(self.watch_dir)):
            logger.info("Ignore move out watched directory: %s", event.src_path)
            return
        self.handle_event_path(Path(dest_path))

    def handle_existing_path(self, source_path: Path, target_path: Path):
        """
        Ensure both files have the same hashes and remove the source file should that be the case

        :param source_path:
        :param target_path:
        :return:
        """
        if target_path.is_dir():
            logger.warning("Target %s of %s is a directory!", target_path, source_path)
            raise NewPathExistsError()

        # Calculate and compare hashes
        source_hasher = hashlib.sha3_512()
        source_hasher.update(source_path.read_bytes())
        target_hasher = hashlib.sha3_512()
        target_hasher.update(target_path.read_bytes())
        if source_hasher.digest() != target_hasher.digest():
            raise NewPathExistsError()

        # Remove the target file and keep the existing file since they're the same
        source_path.unlink(missing_ok=True)
        logger.info("Removed %s and kept %s", source_path, target_path)
        self.call_event_listener(Event.KEPT_SAME_FILE, str(source_path), str(target_path))
