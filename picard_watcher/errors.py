"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from enum import Enum
from typing import Iterable


class ErrorID(str, Enum):
    DEFAULT = "Default"
    BAD_FILE = "BadFile"
    NO_TAGS = "NoTags"
    MISSING_REQUIRED_TAGS = "MissingRequiredTags"
    NEW_PATH_EXISTS = "NewPathExists"
    NO_NEW_PATH = "NoNewPath"


class PicardWatcherError(Exception):
    """An error specific to the application"""

    code: ErrorID
    """The ID or code that can be used to handle the error"""

    def __str__(self):
        return self.__doc__ or super().__str__()


class BadFileError(PicardWatcherError):
    """The input file is not a music file"""
    code = ErrorID.BAD_FILE


class NoTagsError(BadFileError):
    """The input file has no music tags"""
    code = ErrorID.NO_TAGS


class MissingRequiredTagsError(BadFileError):
    """THe input file is missing some required tags"""
    code = ErrorID.MISSING_REQUIRED_TAGS

    def __init__(self, required_tags: Iterable[str]):
        self.required_tags = required_tags

    def __str__(self):
        return f"Missing required tags: {', '.join(self.required_tags)}"


class NewPathError(PicardWatcherError):
    """There's a problem with the new path that was calculated"""


class NewPathExistsError(NewPathError):
    """The calculated new path already exists and won't be overwritten"""
    code = ErrorID.NEW_PATH_EXISTS


class NoNewPathError(NewPathError):
    """No new path could be calculated"""

    code = ErrorID.NO_NEW_PATH
