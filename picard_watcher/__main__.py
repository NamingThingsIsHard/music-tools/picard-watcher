"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import signal
from pathlib import Path
from typing import List, Optional

from watchdog.observers import Observer

from picard_watcher.args import get_parser
from picard_watcher.handler import PicardRenameEventHandler

parser = get_parser()

args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

watch_dir: Path = args.watch_dir
script_path: Path = args.script
target_dir: Path = args.target
required_tags: Optional[List[str]] = args.tags

observer = Observer()
handler = PicardRenameEventHandler(
    watch_dir,
    script_path.read_text(),
    target_dir,
    required_tags=required_tags,
    event_handler=args.handler,
    endings=args.endings,
)
observer.schedule(handler, str(watch_dir), recursive=True)


def handle_SIGTERM(*args):
    observer.stop()


signal.signal(signal.SIGTERM, handle_SIGTERM)
signal.signal(signal.SIGINT, handle_SIGTERM)

logging.info("Watching folder %s", watch_dir)
observer.start()
observer.join()
