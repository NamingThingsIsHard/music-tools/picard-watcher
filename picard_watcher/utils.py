"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import enum
import logging
from pathlib import Path
from typing import Dict, List, Optional, Union

import mutagen
from mutagen import FileType
from picard.metadata import Metadata
from picard.script import ScriptParser

from picard_watcher.errors import BadFileError


def exec_script(script: str, context: Union[dict, Metadata]) -> str:
    """
    Execute a picard script

    https://picard-docs.musicbrainz.org/en/extending/scripting.html
    """
    meta_context = context if isinstance(context, Metadata) else Metadata(**context)
    parser = ScriptParser()
    return parser.eval(script, meta_context).strip()


def get_tags(file_path: Path) -> Optional[Dict[str, List[str]]]:
    """
    Read audio file tags from a given file and return them as a dict
    """

    file = None
    try:
        file: Optional[FileType] = mutagen.File(str(file_path), easy=True)
    except mutagen.MutagenError:
        logging.warning("Couldn't read file %s", file_path, exc_info=True)
    if file is None:
        raise BadFileError()
    if not (tags := file.tags):
        return
    if not (iter_func := getattr(tags, "items", None)):
        return

    return dict(iter_func())


def remove_empty_dirs_until_parent(path: Path, parent: Path):
    abs_path = path.absolute()
    abs_parent = parent.absolute()
    # Ensure parent really is the parent and that path is a directory
    if not (str(abs_path).startswith(str(abs_parent)) and abs_path.is_dir()):
        return

    current_path = abs_path
    # Delete empty parents until the parent (don't want to delete the parent)
    while current_path != parent and len(list(current_path.iterdir())) == 0:
        current_path.rmdir()
        current_path = current_path.parent

class Event(str, enum.Enum):
    ERROR = "error"
    MOVED = "moved"
    KEPT_SAME_FILE = "keptSameFile"