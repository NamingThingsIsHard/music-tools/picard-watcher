import argparse
import unittest
from io import StringIO
from pathlib import Path
from typing import List
from unittest.mock import patch

from picard_watcher.args import get_parser


class BaseTest(unittest.TestCase):
    def setUp(self) -> None:
        self.parser = get_parser()
        self.default_args = [
            "/etc",
            "/etc/passwd",
            "/tmp"
        ]

    def assertBadArg(self, args: List[str], expected_message: str):
        with patch("sys.stderr", new=StringIO()) as fake_err:
            with self.assertRaises(SystemExit):
                self.parser.parse_args(args),
            self.assertIn(expected_message, fake_err.getvalue())


class TestArgs(BaseTest):

    def test_good_args(self):
        self.assertEqual(
            self.parser.parse_args(self.default_args),
            argparse.Namespace(
                watch_dir=Path("/etc"),
                script=Path("/etc/passwd"),
                target=Path("/tmp"),
                verbose=False,
                endings=["mp3", "m4a", "ogg", "flac"],
                handler=None,
                tags=None,
            )
        )

    def test_endings(self):
        args = self.parser.parse_args(["--endings", "a,b,c,d,e,f"] + self.default_args)
        self.assertListEqual(args.endings, ["a", "b", "c", "d", "e", "f"])

    def test_verbose(self):
        self.assertTrue(self.parser.parse_args(["--verbose"] + self.default_args).verbose)

    def test_tags(self):
        args = self.parser.parse_args(["--tag", "artist", "-t", "title"] + self.default_args)
        self.assertListEqual(args.tags, ["artist", "title"])


class TestBadArgs(BaseTest):

    def test_bad_watch_dir(self):
        self.assertBadArg(
            ["/notadirectory", "/etc/passwd", "/etc"],
            "error: argument watch_dir: Expected a existing directory",
        )

    def test_bad_script(self):
        self.assertBadArg(
            ["/", "/notafile", "/"],
            "error: argument script: Expected a existing file",
        )

    def test_bad_target(self):
        self.assertBadArg(
            ["/", "/etc/passwd", "/notadirectory"],
            "error: argument target: Expected a existing directory",
        )


class TestHandler(BaseTest):

    def test_good_handler(self):
        args = self.parser.parse_args(["--handler", "/bin/sh"] + self.default_args)
        self.assertEqual(args.handler, Path("/bin/sh"))

    def test_non_executable_handler(self):
        self.assertBadArg(["--handler", "/etc/passwd"], "Path is not executable")


if __name__ == "__main__":
    unittest.main()
