import shutil
import unittest
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Iterable, Tuple
from unittest.mock import MagicMock, patch

from picard_watcher.errors import BadFileError, ErrorID, MissingRequiredTagsError, NewPathExistsError, NoTagsError
from picard_watcher.handler import PicardRenameEventHandler
from picard_watcher.utils import Event

THIS = Path(__file__)
DIR = THIS.parent
ASSETS = DIR / "assets"


class TestPicardWatcher(unittest.TestCase):
    def setUp(self) -> None:
        self.temp_source_dir = TemporaryDirectory()
        self.source_dir = Path(self.temp_source_dir.name)

        self.temp_target_dir = TemporaryDirectory()
        self.target_dir = Path(self.temp_target_dir.name)

        self.handler = PicardRenameEventHandler(self.source_dir, "%artist%/%title%", self.target_dir)

    def tearDown(self) -> None:
        self.temp_source_dir.cleanup()
        self.temp_target_dir.cleanup()

    def _get_handler(self, script: str, asset_name: str) -> Tuple[PicardRenameEventHandler, Path]:
        asset_path = ASSETS / asset_name
        source_path = self.source_dir / asset_path.name
        shutil.copyfile(asset_path, source_path)
        handler = PicardRenameEventHandler(self.source_dir, script, self.target_dir)
        return handler, source_path

    def test_handling_file(self):
        handler, source_path = self._get_handler("%artist%/%title%", "Tea K Pea - lemoncholy.mp3")
        handler.handle_file(source_path)

        target_path = self.target_dir / "Tea K Pea" / "Lemoncholy.mp3"
        self.assertTrue(target_path.exists())

    def test_bad_file(self):
        handler = PicardRenameEventHandler(self.source_dir, "%artist%/%title%", self.target_dir)
        bad_file = self.source_dir / "a bad file"
        bad_file.write_text("something")

        with self.assertRaises(BadFileError):
            handler.handle_file(bad_file)

    def test_no_tags(self):
        # https://www.royaltyfreemusicclips.com/pir/libs/media/HHH1.wav
        handler, source_path = self._get_handler("%artist%/%title%", "HHH1.m4a")

        with self.assertRaises(NoTagsError):
            handler.handle_file(source_path)

    def test_missing_tags(self):
        handler, source_path = self._get_handler("%artist%/%title%", "Tea K Pea - lemoncholy.mp3")
        handler.required_tags = [
            "I don't exist",
            "sometag",
        ]
        with self.assertRaises(MissingRequiredTagsError):
            handler.handle_file(source_path)

    def test_new_path_is_directory(self):
        """
        An existing path that's a directory should trigger an error
        """
        handler, source_path = self._get_handler("%artist%/%title%", "Tea K Pea - lemoncholy.mp3")
        with self.assertRaises(NewPathExistsError):
            (handler.target_dir / "Tea K Pea" / "Lemoncholy.mp3").mkdir(parents=True, exist_ok=True)
            handler.handle_file(source_path)

    def test_new_path_is_different_file(self):
        """
        An existing file with a different hash should trigger an error
        """
        handler, source_path = self._get_handler("%artist%/%title%", "Tea K Pea - lemoncholy.mp3")
        with self.assertRaises(NewPathExistsError):
            target_path = handler.target_dir / "Tea K Pea" / "Lemoncholy.mp3"
            target_path.parent.mkdir(parents=True, exist_ok=True)
            target_path.write_text("This is just different text")
            handler.handle_file(source_path)

    def test_new_path_is_same_file(self):
        handler, source_path = self._get_handler("%artist%/%title%", "Tea K Pea - lemoncholy.mp3")

        # Copy the file to where the watcher will try to put it
        target_path = handler.target_dir / "Tea K Pea" / "Lemoncholy.mp3"
        target_path.parent.mkdir(parents=True, exist_ok=True)

        shutil.copyfile(source_path, target_path)

        handler.handle_file(source_path)

        # Make sure the source was deleted and the target was left
        self.assertFalse(source_path.exists())
        self.assertTrue(target_path.exists())

    def test_handling_error(self):
        # Create a missing required tags problem
        filename = "Tea K Pea - lemoncholy.mp3"
        handler, source_path = self._get_handler("%artist%/%title%", filename)
        script_path = Path("/bin/sh")
        handler.event_handler = script_path
        handler.required_tags = ["sometag"]

        # Mock the subprocess call to make sure we know the handler is called
        mock = MagicMock()
        mock.return_value = 0
        with patch('subprocess.call', new=mock):
            # Trigger the error situation
            handler.handle_event_path(source_path)

            # Ensure an attempt to call the script was made with the right args
            self.assertEqual(mock.call_count, 1)
            self.assertEqual(len(mock.call_args.args), 1)
            self.assertEqual(
                mock.call_args.args[0],
                [
                    script_path,
                    Event.ERROR.value,
                    ErrorID.MISSING_REQUIRED_TAGS.value,
                    "Missing required tags: sometag",
                    filename,
                ]
            )


class TestEndingsOption(unittest.TestCase):
    """Cases to test the endings=Iterable parameter"""

    def _test_ending(self, endings: Iterable[str], filename: str, expected_call_count: int):
        """Given possible endings and a filename, ensure that "handle_file" was called X times"""
        handler = PicardRenameEventHandler(Path("/"), "", Path("/"), endings=endings)
        with patch.object(handler, "handle_file", autospec=True) as handle_mock:
            handler.handle_event_path(Path(filename))

            self.assertEqual(handle_mock.call_count, expected_call_count)

    def test_good_ending(self):
        self._test_ending(["mp3"], "a real.mp3", 1)

    def test_bad_ending(self):
        self._test_ending(["flac", "m4a", "ogg"], "a real.mp3", 0)


if __name__ == '__main__':
    unittest.main()
