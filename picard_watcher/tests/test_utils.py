"""
picard-watcher
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
from pathlib import Path
from tempfile import TemporaryDirectory

from picard.metadata import Metadata
from picard.script import ScriptEndOfFile, ScriptSyntaxError

from picard_watcher.errors import BadFileError
from picard_watcher.utils import exec_script, get_tags, remove_empty_dirs_until_parent

THIS = Path(__file__)
DIR = THIS.parent
ASSETS = DIR / "assets"


class TestGetTags(unittest.TestCase):
    def test_good_file(self):
        mp3_path = ASSETS / "Tea K Pea - lemoncholy.mp3"
        tags = get_tags(mp3_path)
        self.assertDictEqual(
            tags, {
                'artist': ['Tea K Pea'],
                'bpm': ['112'],
                'date': ['2021'],
                'encodedby': ['LAME in FL Studio 20'],
                'title': ['Lemoncholy'],
            }
        )

    def test_bad_file(self):
        with self.assertRaises(BadFileError):
            get_tags(THIS)

    def test_no_tags(self):
        self.assertIsNone(get_tags(ASSETS / "HHH1.m4a"))


class TestExecScript(unittest.TestCase):

    def test_dict_context(self):
        res = exec_script(
            "%artist% - %title%", {
                "artist": "My Artist",
                "title": "A Title",
            }
        )
        self.assertEqual(res, "My Artist - A Title")

    def test_bad_script(self):
        with self.assertRaises(ScriptEndOfFile):
            exec_script("%artist", {})
        with self.assertRaises(ScriptSyntaxError):
            exec_script("%artist\n", {})

    def test_metadata_context(self):
        res = exec_script(
            "%artist% - %title%", Metadata(
                **{
                    "artist": "My Artist",
                    "title": "A Title",
                }
            )
        )
        self.assertEqual(res, "My Artist - A Title")


class TextExecComplicatedScript(unittest.TestCase):

    def setUp(self) -> None:
        self.script = """
        $set(_track,$if(%tracknumber%,$num($rreplace(%tracknumber%,/.*,),2) - ,))
        $set(_album,$if(%album%,%album%/,))
        
        %artist%/%_album%%_track%%title%
        """

    def test_no_album(self):
        self.assertEqual(
            exec_script(
                self.script, {
                    "artist": "My Artist",
                    "title": "A Title",
                }
            ), "My Artist/A Title"
        )

    def test_tracknumber(self):
        self.assertEqual(
            exec_script(
                self.script, {
                    "artist": "My Artist",
                    "title": "A Title",
                    "tracknumber": "2/5"
                }
            ), "My Artist/02 - A Title"
        )

    def test_with_album(self):
        self.assertEqual(
            exec_script(
                self.script, {
                    "artist": "My Artist",
                    "album": "An Album",
                    "title": "A Title",
                    "tracknumber": 2
                }
            ), "My Artist/An Album/02 - A Title"
        )


class RemoveEmptyDirsUntilParentTests(unittest.TestCase):

    def setUp(self) -> None:
        self.test_dir = TemporaryDirectory()
        self.test_dir_path = Path(self.test_dir.name)

    def tearDown(self) -> None:
        self.test_dir.cleanup()

    def test_empty_tree(self):
        path = (self.test_dir_path / "sub" / "other sub" / "yet another")
        path.mkdir(parents=True)
        remove_empty_dirs_until_parent(path, self.test_dir_path)

        self.assertFalse(path.exists())
        self.assertTrue(self.test_dir_path.exists())

    def test_non_empty_tree(self):
        path = (self.test_dir_path / "sub" / "other sub" / "yet another")
        path.mkdir(parents=True)
        test_file = path.parent / "file"
        test_file.write_text("")

        remove_empty_dirs_until_parent(path, self.test_dir_path)

        self.assertFalse(path.exists())
        self.assertTrue(path.parent.exists())
        self.assertTrue(self.test_dir_path.exists())

    def test_same_as_parent(self):
        remove_empty_dirs_until_parent(self.test_dir_path, self.test_dir_path)
        self.assertTrue(self.test_dir_path.exists())

    def test_file(self):
        path = (self.test_dir_path / "sub" / "other sub" / "afile.txt")
        path.parent.mkdir(parents=True)
        path.write_text("I exist")

        remove_empty_dirs_until_parent(path, self.test_dir_path)
        self.assertTrue(path.exists())
        self.assertTrue(self.test_dir_path.exists())


if __name__ == '__main__':
    unittest.main()
