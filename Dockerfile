FROM python:3.9-slim as base

WORKDIR /app

RUN pip install poetry \
    && poetry config virtualenvs.create false \
    && apt-get update -qq \
    && apt-get install -yq  \
        gettext \
        qt5-qmake \
        gcc \
        libglib2.0-0 \
    && rm -rf /var/lib/apt/lists/*

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev --no-root


FROM base as ci
RUN poetry install --no-root

FROM base as prod

VOLUME /scripts
VOLUME /target
VOLUME /watched

COPY . ./

ENTRYPOINT [ "python3", "picard_watcher" ]
CMD "--help"
